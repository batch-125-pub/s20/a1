let arr = [
	{
		id: 111,
		name: "English",
		description: "English_desc",
		price: 1000,
		isActive: true
	},
	{
		id: 222, 
		name: "Programming", 
		description: "Programming_desc", 
		price: 800, 
		isActive: true
	},
	{
		id: 333, 
		name: "Math", 
		description: "Math_desc", 
		price: 500, 
		isActive: true
	},
	{
		id: 444, 
		name: "Music", 
		description: "Music_desc",
		price: 800,
		isActive: true
	},
	{
		id: 555, 
		name: "P.E.", 
		description: "P.E_desc",
		price: 700,
		isActive: true
	}


];

let addCourse = (id, name, desc, price, isActive) => {
	let course = {};
	course.id = id;
	course.name = name;
	course.desc = desc;
	course.price = price;
	course.isActive = isActive;
	arr.push(course);
	alert(`You have created ${course.name}. Its price is ${course.price}`);
	return arr;
}

let retrieveCourse = (id) => {
	let foundCourse = arr.find((element) =>  element.id == id)
	return foundCourse;
}

let archiveCourse = (name) => {
	let archivedCourse = arr.findIndex((element) => element.name == name)
	console.log(arr[archivedCourse].isActive);
	arr[archivedCourse].isActive = false;
	console.log(`${arr[archivedCourse].name}'s active status is set to ${arr[archivedCourse].isActive}`);
}

let printAll = () => {
	let courseItem = arr.forEach((element) =>
		{
			return console.log("Item", element);

		}
	)
}

let deleteCourse = () => {
	arr.pop();
}

let filterCourses = () => {
		let courseList = arr.filter((element) => 
			{
				return element.isActive == true;
			}
		)
		return courseList;
}

addCourse(777, "TLE", "TLE_desc", 999, true);
console.log("After adding", arr);
deleteCourse();
console.log("After deletion", arr);
console.log("Course with id: 222",retrieveCourse(222));
printAll();
console.log(arr);
console.log("Filtered courses", filterCourses());
archiveCourse("Math");
console.log("Filtered courses", filterCourses());

